package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс для маппинга стран в выходной документ.
 */
public class Country implements Serializable {

    /**
     * Поле countryName.
     *
     * name - аттрибут и название страны, соответствующее
     * элементу COUNTRY  из исходного файла cd_catalog.xml.
     */
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    @JsonProperty("Country Name")
    private String countryName;

    /**
     * Список исполнителей.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Artist")
    @JsonProperty("Artists")
    private List<Artist> artist = new ArrayList<>();

    /**
     * Конструктор по-умолчанию для страны.
     */
    public Country() {
    }


    /**
     * Конструктор для страны с входящим аргументом - названием страны.
     *
     * @param countryNameIncome - название страны.
     */
    public Country(final String countryNameIncome) {
        this.countryName = countryNameIncome;
    }

    /**
     * Получение имени исполнителя.
     *
     * @return artist - имя исполнителя.
     */
    public final List<Artist> getArtist() {
        return artist;
    }

    /**
     * Получение названия страны.
     *
     * @return countryName - название страны.
     */
    public final String getCountryName() {
        return countryName;
    }
}
