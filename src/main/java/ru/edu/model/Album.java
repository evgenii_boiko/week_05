package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Optional;

/**
 * Класс для маппинга альбомов в выходной документ.
 */
public class Album implements Serializable {

    /**
     * Поле albumTitle.
     *
     * name - аттрибут и название альбома, соответствующее элементу
     * TITLE  из исходного файла cd_catalog.xml
     */
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    @JsonProperty("name")
    private String albumTitle;

    /**
     * Поле albumManufactureYear.
     *
     * year - аттрибут и год выхода альбома, соответствующий элементу YEAR
     * из исходного файла cd_catalog.xml
     */
    @JacksonXmlProperty(localName = "year", isAttribute = true)
    @JsonProperty("year")
    private int albumManufactureYear;

    /**
     * Опциональный объект cd.
     *
     * Благодаря такому объявлению, данный объект может содержаться,
     * а может и не содержаться.
     */
    @JsonIgnore
    @XmlTransient
    private transient Optional<CD> cd;

    /**
     * Конструктор по-умолчанию для альбома.
     */
    public Album() {
    }

    /**
     * Конструктор для альбома с аргументами.
     *
     * @param albumTitleIncome - название альбома
     * @param albumManufactureYearIncome - год выхода альбома
     * @param cdIncome - опциональный объект cd
     */
    public Album(final String albumTitleIncome,
                 final int albumManufactureYearIncome, final CD cdIncome) {
        this.albumTitle = albumTitleIncome;
        this.albumManufactureYear = albumManufactureYearIncome;
        this.cd = Optional.ofNullable(cdIncome);
    }


    /**
     * Конструктор для альбома с аргументами.
     *
     * @param albumTitleIncome - название альбома
     * @param albumManufactureYearIncome - год выхода альбома
     */
    public Album(final String albumTitleIncome,
                 final int albumManufactureYearIncome) {
        this(albumTitleIncome, albumManufactureYearIncome, null);
    }

    /**
     * Получение названия альбома.
     *
     * @return albumTitle
     */
    public String getAlbumTitle() {
        return albumTitle;
    }

    /**
     * Получение года выхода альбома.
     *
     * @return albumManufactureYear
     */
    public int getAlbumManufactureYear() {
        return albumManufactureYear;
    }

    /**
     *
     * Получение имени исполнителя.
     *
     * Здесь метод orElse() позволяет определить альтернативное значение,
     * которое будет возвращаться, если Optional
     * не получит из потока какого-нибудь значения.
     *
     * Эта конструкция гарантированно вернёт нам объект cd
     * класса CD, хоть даже и пустой.
     *
     * @return cd.orElse(new CD()).getArtistName()
     */
    @JsonIgnore
    public String getArtistName() {
        return cd.orElse(new CD()).getArtistName();
    }

    /**
     * Получение названия страны.
     *
     * Здесь метод orElse() позволяет определить альтернативное значение,
     * которое будет возвращаться, если Optional не получит из потока
     * какого-нибудь значения
     *
     * Эта конструкция гарантированно вернёт нам объект cd
     * класса CD, хоть даже и пустой.
     *
     * @return cd.orElse(new CD()).getCountryName()
     */
    @JsonIgnore
    public String getCountryName() {
        return cd.orElse(new CD()).getCountryName();
    }
}
