package ru.edu.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс-маппинг для корневого свойства
 * CATALOG из исходного файла cd_catalog.xml.
 */
@JacksonXmlRootElement(localName = "CATALOG")
public class Catalogue {

    /**
     * Список всех элементов CD из файла
     * cd_catalog.xml в виде ArrayList'а.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
            @JacksonXmlProperty(localName = "CD")
    private final List<CD> cdList = new ArrayList<>();


    /**
     * Конструктор по-умолчанию.
     *
     * Jackson'у во время десериализации из строк в объект потребуется создать
     * пустой объект, а потом уже его наполнять.
     * Поэтому требуется наличие конструктора
     * по-умолчанию (конструктора без аргументов).
     */
    public Catalogue() {
    }

    /**
     * Получение списка CD.
     *
     * @return cdList - список CD
     */
    public final List<CD> getCdList() {
        return cdList;
    }
}
