package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс для маппинга исполнителей в выходной документ.
 */
//@JsonPropertyOrder({"Artist's name", "Album"}) - можно раскомментировать,
// чтобы был правильный порядок элементов в JSON,
// но тогда, почему-то меняется порядок этих же полей в XML
public class Artist implements Serializable {

    /**
     * Поле artistName.
     *
     * Name - имя исполнителя, соответствующее элементу ARTIST
     * из исходного файла cd_catalog.xml
     */
    @JacksonXmlProperty(localName = "Name")
    @JsonProperty("Artist's name")
    private String artistName;

    /**
     * Список альбомов (с названием и годом выхода).
     *
     * Здесь мы используем обертку Albums, уже внутри которой будут
     * располагаться наши альбомы данного конкретного исполнителя.
     */
    @JacksonXmlElementWrapper(localName = "Albums")
    @JacksonXmlProperty(localName = "Album")
    @JsonProperty("Albums")
    private List<Album> albums = new ArrayList<>();

    /**
     * Конструктор по-умолчанию для исполнителя.
     */
    public Artist() {
    }


    /**
     * Конструктор для исполнителя с входящим аргументом - именем исполнителя.
     *
     * @param artistNameIncome - имя исполнителя.
     */
    public Artist(final String artistNameIncome) {
        this.artistName = artistNameIncome;
    }

    /**
     * Getter для получения для получения имени исполнителя из реестра.
     *
     * @return artistName - имя исполнителя, соответствующее
     * элементу ARTIST из исходного файла cd_catalog.xml.
     */
    public String getArtistName() {
        return artistName;
    }

    /**
     * Getter для получения списка альбомов из реестра.
     *
     * @return albums - список альбомов из реестра.
     */
    public List<Album> getAlbums() {
        return albums;
    }
}
