package ru.edu.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс-реестр исполнителей.
 */
@JacksonXmlRootElement(localName = "ArtistRegistry")
@JsonRootName("ArtistRegistry")
public class ArtistRegistry implements Serializable {

    /**
     * Аттрибут countryCount у свойства ArtistRegistry.
     *
     * countryCount - счетчик и порядковый номер страны в реестре
     */
    @JacksonXmlProperty(localName = "countryCount", isAttribute = true)
    @JsonProperty("Countries' Count")
    private int countryCount;



    /**
     * Список стран.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Country")
    @JsonProperty("Countries")
    private List<Country> countryName = new ArrayList<>();

    /**
     * Getter для получения списка стран из реестра.
     *
     * @return countries - список стран из реестра
     */
    public List<Country> getCountryName() {
        return countryName;
    }

    /**
     * Добавление страны в список.
     *
     * @param country - название страны
     */
    public void addCountry(final Country country) {
        countryName.add(country);
    }

    /**
     * Добавление стран.
     *
     * @param artistRegistry - объект реестра ArtistRegistry
     */
    public void addCountryAll(final ArtistRegistry artistRegistry) {
        countryName.addAll(artistRegistry.getCountryName());
    }

    /**
     * @return countryCount
     */
    public int getCountryCount() {
        int a;
        a = getCountryName().size();
        countryCount = a;
        return countryCount;
    }

    @Override
    public final String toString() {
        return "ArtistRegistry{"
                + "countryCount=" + countryCount
                + ", countryName=" + countryName
                + '}';
    }
}
