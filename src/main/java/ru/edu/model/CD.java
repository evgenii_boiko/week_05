package ru.edu.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


/**
 * Класс-маппинг для свойства CD
 * из исходного файла cd_catalog.xml.
 */
public class CD {

    /**
     * Поле albumTitle.
     *
     * TITLE - название альбома из исходного файла cd_catalog.xml.
     */
    @JacksonXmlProperty(localName = "TITLE")
    private String albumTitle;

    /**
     * Поле artistName.
     *
     * ARTIST - имя исполнителя из исходного файла cd_catalog.xml.
     */
    @JacksonXmlProperty(localName = "ARTIST")
    private String artistName;

    /**
     * Поле countryName.
     *
     * COUNTRY - название страны из исходного файла cd_catalog.xml
     */
    @JacksonXmlProperty(localName = "COUNTRY")
    private String countryName;

    /**
     * Поле companyName.
     *
     * COMPANY - название компании-изготовителя из
     * исходного файла cd_catalog.xml
     */
    @JacksonXmlProperty(localName = "COMPANY")
    private String companyName;

    /**
     * Поле cdPrice.
     *
     * PRICE - стоимость диска из
     * исходного файла cd_catalog.xml
     */
    @JacksonXmlProperty(localName = "PRICE")
    private double cdPrice;

    /**
     * Поле albumManufactureYear.
     *
     * YEAR - год выхода альбома из
     * исходного файла cd_catalog.xml
     */
    @JacksonXmlProperty(localName = "YEAR")
    private int albumManufactureYear;

    /**
     * Получение названия альбома.
     *
     * @return albumTitle - название альбома
     */
    public final String getAlbumTitle() {
        return albumTitle;
    }

    /**
     * Получение имени исполнителя.
     *
     * @return artistName - имя исполнителя
     */
    public final String getArtistName() {
        return artistName;
    }

    /**
     * Получение названия страны.
     *
     * @return countryName - название страны
     */
    public final String getCountryName() {
        return countryName;
    }

    /**
     * Получение названия компании.
     *
     * @return companyName - название компании
     */
    public final String getCompanyName() {
        return companyName;
    }

    /**
     * Получение цены альбома.
     *
     * @return getCdPrice - цена альбома
     */
    public final double getCdPrice() {
        return cdPrice;
    }

    /**
     * Получение даты выхода альбома.
     *
     * @return albumManufactureYear - дата выхода альбома
     */
    public final int getAlbumManufactureYear() {
        return albumManufactureYear;
    }

}
