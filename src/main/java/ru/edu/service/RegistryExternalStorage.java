package ru.edu.service;

import ru.edu.model.ArtistRegistry;
import ru.edu.model.Catalogue;

/**
 * Интерфейс для сериализации/десериализации.
 *
 * Под каждый формат XML, JSON, java serialization
 * существует своя отдельная реализация.
 *
 * В данном интерфейсе два метода - readFrom
 * (для чтения исходного XML-файла) и writeTo (для записи в выходной файл).
 *
 */
public interface RegistryExternalStorage {

    /**
     * Чтение исходного XML-файла.
     *
     * @param filePath путь до исходного XML-файла
     * @return прочитанный файл
     */
    Catalogue readFrom(String filePath);

    /**
     * Запись ArtistRegistry в выходной файл.
     *
     * @param filePath путь
     * @param artistRegistry реестр
     */
    void writeTo(String filePath, ArtistRegistry artistRegistry);
}
