package ru.edu.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.edu.model.ArtistRegistry;
import ru.edu.model.Catalogue;

import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Класс для сериализации/десериализации.
 *
 * Данный класс реализует интерфейс RegistryExternalStorage
 * для итоговой десериализации в XML.
 *
 * В данном классе два метода - readFrom
 * (для чтения исходного XML-файла) и writeTo (для записи в выходной файл).
 *
 */
public class RegistryExternalStorageXMLImpl implements
        RegistryExternalStorage {

    /**
     * Чтение исходного XML-файла.
     *
     * @param filePath путь до исходного XML-файла
     */
    @Override
    public Catalogue readFrom(final String filePath) {

        ObjectMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature
                .FAIL_ON_UNKNOWN_PROPERTIES, false);

        try (FileInputStream fileInputStream =
                     new FileInputStream(filePath)) {
            return mapper.readValue(fileInputStream, Catalogue.class);
        } catch (Exception e) {
            throw new RuntimeException();
        }

    }

    /**
     * Запись ArtistRegistry в выходной файл.
     *
     * @param filePath путь
     * @param artistRegistry реестр
     */
    public void writeTo(final String filePath,
                        final ArtistRegistry artistRegistry) {

        ObjectMapper mapper = new XmlMapper();

        try (FileOutputStream fileOutputStream =
                     new FileOutputStream(filePath)) {
            mapper.writeValue(fileOutputStream, artistRegistry);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
