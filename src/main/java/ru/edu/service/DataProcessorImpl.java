package ru.edu.service;

import ru.edu.model.ArtistRegistry;
import ru.edu.model.Catalogue;

/**
 * Класс, реализующий интерфейс DataProcessor,
 * предназначенный для формирования выходного файла.
 *
 * В его методе processFor[***] последовательно вызываются 3 метода:
 *
 * 1) чтение исходного XML-файла;
 * 2) транcформация (частичный маппинг данных)
 * класса Catalogue в класс ArtistRegistry;
 * 3) запись в выходной файл требуемым форматом.
 */
public class DataProcessorImpl implements DataProcessor {

    /**
     * Путь до входного файла.
     */
    private final String cdCatalogFilepath = "./input/cd_catalog.xml";


    /**
     * Путь до выходного XML-файла.
     */
    private final String artistByCountryXmlFilepath =
            "./output/artist_by_country.xml";

    /**
     * Путь до выходного JSON-файла.
     */
    private final String artistByCountrySerializedFilepath =
            "./output/artist_by_country.serialized";

    /**
     * Путь до выходного Serialized-файла.
     */
    private final String artistByCountryJsonFilepath =
            "./output/artist_by_country.json";


    /**
     * Экземпляр класса с чтением и записью в XML-файл.
     */
    private RegistryExternalStorageXMLImpl registryExternalStorageXML =
            new RegistryExternalStorageXMLImpl();

    /**
     * Экземпляр класса с чтением и записью в JSON-файл.
     */
    private RegistryExternalStorageJSONImpl registryExternalStorageJSON =
            new RegistryExternalStorageJSONImpl();

    /**
     * Экземпляр класса с чтением и записью в Serialized-файл.
     */
    private RegistryExternalStorageSerializedImpl
            registryExternalStorageSerialized =
            new RegistryExternalStorageSerializedImpl();

    /**
     * Экземпляр класса трансформацией
     * класса Catalogue в класс ArtistRegistry.
     */
    private CatalogueToArtistRegistryTransformation
            catalogueToArtistRegistryTransformation =
            new CatalogueToArtistRegistryTransformation();

    @Override
    public final void processForXML() {
        Catalogue catalogueForMapping =
                registryExternalStorageXML.readFrom(cdCatalogFilepath);
        ArtistRegistry artistRegistry =
                catalogueToArtistRegistryTransformation
                        .transformation(catalogueForMapping);
        registryExternalStorageXML
                .writeTo(artistByCountryXmlFilepath, artistRegistry);
    }

    @Override
    public final void processForJSON() {
        Catalogue catalogueForMapping =
                registryExternalStorageJSON.readFrom(cdCatalogFilepath);
        ArtistRegistry artistRegistry =
                catalogueToArtistRegistryTransformation
                        .transformation(catalogueForMapping);
        registryExternalStorageJSON
                .writeTo(artistByCountryJsonFilepath, artistRegistry);
    }

    @Override
    public final void processForSerialized() {
        Catalogue catalogueForMapping =
                registryExternalStorageSerialized.readFrom(cdCatalogFilepath);
        ArtistRegistry artistRegistry =
                catalogueToArtistRegistryTransformation
                        .transformation(catalogueForMapping);
        registryExternalStorageSerialized
                .writeTo(artistByCountrySerializedFilepath, artistRegistry);
    }
}
