package ru.edu.service;



import ru.edu.model.Album;
import ru.edu.model.Artist;
import ru.edu.model.ArtistRegistry;
import ru.edu.model.Catalogue;
import ru.edu.model.Country;

import java.util.HashMap;
import java.util.Map;

/**
 * Класс для трансформации (частичного маппинга данных)
 * класса Catalogue в класс ArtistRegistry.
 *
 * На вход мы получаем наш класс Catalogue,
 * а ны выходе - возвращаем собранный класс ArtistRegistry.
 */
public class CatalogueToArtistRegistryTransformation {

    /**
     * Трансформация (частичный маппинг данных)
     * класса Catalogue в класс ArtistRegistry.
     *
     * @param catalogue - класс Catalogue
     * @return artistRegistry - собранный класс ArtistRegistry
     */
    public final ArtistRegistry transformation(final Catalogue catalogue) {

        ArtistRegistry artistRegistry = catalogue.getCdList().stream()
                .map(cd -> new Album(cd.getAlbumTitle(),
                        cd.getAlbumManufactureYear(), cd))
                .collect(
                        HashMap::new,
                        this::insertAlbum,
                        HashMap::putAll
                )
                .values().stream()
                .collect(
                        HashMap::new,
                        this::insertCountry,
                        HashMap::putAll
                )
                .values().stream()
                .collect(
                        ArtistRegistry::new,
                        ArtistRegistry::addCountry,
                        ArtistRegistry::addCountryAll
                );
        System.out.println(artistRegistry.toString());

        //artistRegistry.countryCount = artistRegistry.getCountryName().size();
        //System.out.println(artistRegistry.countryCount);
        System.out.println(artistRegistry.getCountryCount());

        return artistRegistry;
    }


    /**
     * Метод для создания и обновления словаря исполнителей в реестре.
     *
     * @param map - словарь исполнителей в реестре.
     * @param album - объект-альбом для добавления в реестр -
     *              самый глубокий уровень вложенности в реестре.
     */
    private void insertAlbum(final Map<String, Artist> map,
                             final Album album) {
        map.putIfAbsent(album.getArtistName(),
                new Artist(album.getArtistName()));
        map.get(album.getArtistName()).getAlbums().add(album);
    }

    /**
     * Метод для создания и обновления словаря стран в реестре.
     *
     * @param map - словарь стран в реестре. Страны -
     *            самый низкий (верхний) уровень вложенности в реестре.
     * @param artist объект-исполнитель для добавления в реестр.
     */
    private void insertCountry(final Map<String, Country> map,
                               final Artist artist) {
        String countryName = artist.getAlbums().get(0).getCountryName();
        map.putIfAbsent(countryName, new Country(countryName));
        map.get(countryName).getArtist().add(artist);
    }
}
