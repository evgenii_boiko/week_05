package ru.edu.model;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ArtistRegistryTest {

    @Test
    public void getCountries() throws Exception {

        ArtistRegistry artistRegistry = new ArtistRegistry();
        fillCountries(artistRegistry.getCountryName());

        ObjectMapper mapper = new ObjectMapper();

        try(FileOutputStream fileOutputStream = new FileOutputStream("./output/artist_by_country.json")) {
            mapper.writeValue(fileOutputStream, artistRegistry);

        }
    }

    private void fillCountries(List<Country> countries) {
        for (int i = 0; i < 3; i++) {
            Country country = new Country("Country" + i);

            fillArtist(country.getArtist());
            countries.add(country);
        }
    }

    private void fillArtist(List<Artist> artists) {
        for (int i = 0; i < 3; i++) {
            artists.add(new Artist("Artist " + i));
        }
    }

    @Test
    public void test() {
        ObjectMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        try (FileInputStream fileInputStream = new FileInputStream("./src/test/resources/example_files/artist.xml")) {
            Artist artist = mapper.readValue(fileInputStream, Artist.class);
            assertEquals("Savage Rose", artist.getArtistName());
            assertNotNull(artist.getAlbums());

            String xmlObject = new XmlMapper().writeValueAsString(artist);
            assertEquals("<Artist><Name>Savage Rose</Name><Albums><Album name=\"Black angel\" year=\"1995\"/></Albums></Artist>", xmlObject);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}