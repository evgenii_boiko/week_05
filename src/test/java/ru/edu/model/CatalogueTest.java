package ru.edu.model;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;

import java.io.FileInputStream;


import static org.junit.Assert.*;

public class CatalogueTest {

    @Test
    public void test(){

        ObjectMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        try(FileInputStream fileInputStream = new FileInputStream("./input/cd_catalog.xml")) {
            Catalogue catalogue = mapper.readValue(fileInputStream, Catalogue.class);

            assertNotNull(catalogue);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}