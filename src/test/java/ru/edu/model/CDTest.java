package ru.edu.model;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;

import java.io.FileInputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CDTest {

    @Test
    public void test() {
        ObjectMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        try (FileInputStream fileInputStream = new FileInputStream("./src/test/resources/example_files/cd.xml")) {
            CD cd = mapper.readValue(fileInputStream, CD.class);
            assertEquals("Empire Burlesque", cd.getAlbumTitle());
            assertEquals("Bob Dylan", cd.getArtistName());
            assertEquals("USA", cd.getCountryName());
            assertEquals("Columbia", cd.getCompanyName());
            assertEquals(10.90, cd.getCdPrice(), 0.001);
            assertEquals(1985, cd.getAlbumManufactureYear());


            assertNotNull(cd);

            String xmlObject = new XmlMapper().writeValueAsString(cd);
            assertEquals("<CD><TITLE>Empire Burlesque</TITLE><ARTIST>Bob Dylan</ARTIST><COUNTRY>USA</COUNTRY><COMPANY>Columbia</COMPANY><PRICE>10.9</PRICE><YEAR>1985</YEAR></CD>", xmlObject);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
