package ru.edu.model;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;

import java.io.FileInputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CountryTest {

    @Test
    public void test() {
        ObjectMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        try (FileInputStream fileInputStream = new FileInputStream("./src/test/resources/example_files/country.xml")) {
            Country country = mapper.readValue(fileInputStream, Country.class);
            assertEquals("EU", country.getCountryName());
            assertNotNull(country.getArtist());

            String xmlObject = new XmlMapper().writeValueAsString(country);
            assertEquals("<Country name=\"EU\"><Artist><Name/><Albums/></Artist></Country>", xmlObject);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
