package ru.edu.model;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;

import java.io.FileInputStream;

import static org.junit.Assert.*;

public class ArtistTest {

    @Test
    public void test() {
        ObjectMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        try (FileInputStream fileInputStream = new FileInputStream("./src/test/resources/example_files/artist.xml")) {
            Artist artist = mapper.readValue(fileInputStream, Artist.class);
            assertEquals("Savage Rose", artist.getArtistName());
            assertNotNull(artist.getAlbums());

            String xmlObject = new XmlMapper().writeValueAsString(artist);
            assertEquals("<Artist><Name>Savage Rose</Name><Albums><Album name=\"Black angel\" year=\"1995\"/></Albums></Artist>", xmlObject);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}