package ru.edu.model;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;

import java.io.FileInputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AlbumTest {

    @Test
    public void test() {
        ObjectMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        try (FileInputStream fileInputStream = new FileInputStream("./src/test/resources/example_files/album.xml")) {
            Album album = mapper.readValue(fileInputStream, Album.class);
            assertEquals("Black angel", album.getAlbumTitle());
            assertEquals(1995, album.getAlbumManufactureYear());

            assertNotNull(album);

            String xmlObject = new XmlMapper().writeValueAsString(album);
            assertEquals("<Album name=\"Black angel\" year=\"1995\"/>", xmlObject);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
