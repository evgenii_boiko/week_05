package ru.edu;

import org.junit.Test;
import ru.edu.service.DataProcessor;
import ru.edu.service.DataProcessorImpl;


/**
 * Класс для тестирования получения выходного файла.
 *
 * В данном классе в методе для получения выходного файла формата XML, JSON, java serialization
 * вызывается свой отдельный метод - processFor[***] - , который реализует всю логику.
 */
public class RegistryExternalStorageImplTest {

    DataProcessor dataProcessor = new DataProcessorImpl();

    @Test
    public void OutputFileCreating() {
        dataProcessor.processForXML();
        dataProcessor.processForJSON();
        dataProcessor.processForSerialized();
    }
}
